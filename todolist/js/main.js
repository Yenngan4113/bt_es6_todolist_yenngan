import { renderAddedTask, renderCheckedTask } from "./controler/controller.js";
let taskList = [];
let checkedTaskList = [];

// render ra ds task thêm vào
const renderTaskList = (list) => {
  let contentHTML = "";
  for (let i = 0; i < list.length; i++) {
    let contentTask = renderAddedTask(list[i], i);
    contentHTML += contentTask;
  }
  document.querySelector("#todo").innerHTML = contentHTML;
};

// render ra ds các task đã được check
const renderCheckedTaskList = (list) => {
  let contentHTML = "";
  for (let i = 0; i < list.length; i++) {
    let contentTask = renderCheckedTask(list[i], i);
    contentHTML += contentTask;
  }
  document.querySelector("#completed").innerHTML = contentHTML;
};

// save ds task được thêm vào xuống local storage
let taskListStorage = "taskListStorage";
const saveDataToStorage = () => {
  let taskListJSon = JSON.stringify(taskList);
  localStorage.setItem(taskListStorage, taskListJSon);
};

// render 1 lần lúc tải lại trang các task
let taskListJSon = localStorage.getItem(taskListStorage);
if (taskListJSon) {
  taskList = JSON.parse(taskListJSon);
  renderTaskList(taskList);
}

// save ds task được checked xuống local storage
let checkedTaskListStorage = "checkedTaskListStorage";
const saveChekedDataToStorage = () => {
  let checkedTaskListJSon = JSON.stringify(checkedTaskList);
  localStorage.setItem(checkedTaskListStorage, checkedTaskListJSon);
};

// render 1 lần lúc tải lại trang các task đã checked
let checkedTaskListJSon = localStorage.getItem(checkedTaskListStorage);
if (checkedTaskListJSon) {
  checkedTaskList = JSON.parse(checkedTaskListJSon);
  renderCheckedTaskList(checkedTaskList);
}
document.querySelector("#newTask").addEventListener("focus", () => {
  document.querySelector("#todo").style.display = "block";
});
document.querySelector("#addItem").addEventListener("click", () => {
  let inputValue = document.querySelector("#newTask").value;
  inputValue = inputValue[0].toUpperCase() + inputValue.substring(1);
  taskList.push(inputValue);
  saveDataToStorage();
  renderTaskList(taskList);
  document.querySelector("#newTask").value = "";
});

// delete task
const deleteTask = (id) => {
  taskList.splice(id, 1);
  saveDataToStorage();
  renderTaskList(taskList);
};
window.deleteTask = deleteTask;

// nhấn checked render ra danh sách checked và xóa task trong taskList
const checkedTask = (id) => {
  checkedTaskList.unshift(taskList[id]);
  saveChekedDataToStorage();
  renderCheckedTaskList(checkedTaskList);
  taskList.splice(id, 1);
  saveDataToStorage();
  renderTaskList(taskList);
};
window.checkedTask = checkedTask;

// deleted checked task
const deleteCheckedTask = (id) => {
  checkedTaskList.splice(id, 1);
  saveChekedDataToStorage();
  renderCheckedTaskList(checkedTaskList);
};
window.deleteCheckedTask = deleteCheckedTask;

// redo checked task
const undoCheckTask = (id) => {
  taskList.push(checkedTaskList[id]);
  saveDataToStorage();
  renderTaskList(taskList);
  checkedTaskList.splice(id, 1);
  saveChekedDataToStorage();
  renderCheckedTaskList(checkedTaskList);
};
window.undoCheckTask = undoCheckTask;
document.querySelector("#one").addEventListener("click", () => {
  document.querySelector("#todo").style.display = "none";
});
document.querySelector("#two").addEventListener("click", () => {
  taskList.sort((a, b) => {
    return a.localeCompare(b);
  });
  renderTaskList(taskList);
});

document.querySelector("#three").addEventListener("click", () => {
  taskList.sort((a, b) => {
    return b.localeCompare(a);
  });
  renderTaskList(taskList);
});
