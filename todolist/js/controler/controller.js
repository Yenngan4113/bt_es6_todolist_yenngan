export const renderAddedTask = (value, id) => {
  return /*html*/ `<li class="task d-flex justify-content-between">${value}
  <div>
        <i class="fa fa-trash " onClick="deleteTask('${id}')"></i>
        <i class="fa fa-check-circle task_${id}" onClick="checkedTask('${id}')"></i>
        </div>
        </li>`;
};

export const renderCheckedTask = (value, id) => {
  return /*html*/ `<li class="task d-flex justify-content-between">${value}
    <div>
          <i class="fa fa-trash " onClick="deleteCheckedTask('${id}')"></i>
          <i class="fa fa-check-circle text-success" onClick="undoCheckTask('${id}')"></i>
          </div>
          </li>`;
};
